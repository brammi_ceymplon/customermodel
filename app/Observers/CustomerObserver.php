<?php

namespace App\Observers;

use App\Customer;
use App\User;
use Illuminate\Support\Facades\Auth;

class CustomerObserver
{
    public function creating(Customer $customer){

        $customer->user_id = auth()->user()->id;
    }

    public function updating(Customer $customer){

        if($customer->user_id == auth()->user()->id)
            return true;
        else
            return  false ;
    }
    public function updated(Customer $customer){

        if($customer->user_id == auth()->user()->id)
            return true;

    }

    public function restoring(Customer $customer){

        if($customer->user_id == auth()->user()->id)
            return true;
        else
            return false;

    }
    public function restored(Customer $customer){
        if($customer->user_id == auth()->user()->id)
            return true;
        else
            return false;    }
    public function deleting(Customer $customer){
        if($customer->user_id == auth()->user()->id)
            return true;
        else
            return false;
    }
    public function deleted(Customer $customer){

        if($customer->user_id == auth()->user()->id)
            return true;
        else
            return false;
    }
    public function forceDeleted(Customer $customer){

        if($customer->user_id == auth()->user()->id)
            return true;
        else
            return false;
    }
}
